Material Favorite Button
========================

Material spinning favorite/star/like button.

![Demo Image][1]

Download
--------

```groovy
compile 'com.rilixtech.widget.materialfavoritebutton:materialfavoritebutton:1.0.1'
```

Usage
-----

Declare in XML (see xml attributes below for customization):

```xml
<com.rilixtech.widget.materialfavoritebutton.MaterialFavoriteButton
    android:layout_width="wrap_content"
    android:layout_height="wrap_content" />
```

Or static initializer (see xml attributes below for customization):

```
MaterialFavoriteButton favorite = new MaterialFavoriteButton.Builder(this)
        .create();
```



Configure using xml attributes or setters in code:

```
app:mfvb_state="false"                            // default button state
app:mfvb_animate_favorite="true"                  // to animate favoriting
app:mfvb_animate_unfavorite="false"               // to animate unfavoriting
app:mfvb_padding="12"                             // image padding
app:mfvb_favorite_image="@drawable/ic_fav"        // custom favorite resource
app:mfvb_not_favorite_image="@drawable/ic_not_fav"// custom not favorite resource
app:mfvb_rotation_duration="400"                  // rotation duration
app:mfvb_rotation_angle="360"                     // rotation angle
app:mfvb_bounce_duration="300"                    // bounce duration
app:mfvb_color="black"                            // black or white default resources (enum)
app:mfvb_type="star"                              // star or heart shapes (enum)
app:mfvb_size="48"                                // button size
```

Make sure:
 - you are using either (`mfvb_favorite_image` and `mfvb_not_favorite_image`) or (`mfvb_color` and `mfvb_type`).
 - if you change `mfvb_size` attribute you should also provide your own combination of `mfvb_favorite_image` and `mfvb_favorite_image` resources and `mfvb_padding` attribute that will fit your new dimensions, otherwise you can get blurred icon


Set an `OnFavoriteChangeListener` to `MaterialFavoriteButton`:

```
favorite.setOnFavoriteChangeListener(
        new MaterialFavoriteButton.OnFavoriteChangeListener() {
          @Override
          public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {
          	//
          }
        });
```

Set an `OnFavoriteAnimationEndListener` to `MaterialFavoriteButton`:

```
favorite.setOnFavoriteAnimationEndListener(
        new MaterialFavoriteButton.OnFavoriteAnimationEndListener() {
          @Override
          public void onAnimationEnd(MaterialFavoriteButton buttonView, boolean favorite) {
            //
          }
        });
```

##### Usage in RecyclerView
To avoid triggering animation while re-rendering item view make sure you set favorite button state in `onBindViewHolder` without animation:

```
favoriteButton.setFavorite(isFavorite(data.get(position)));
```


Developed By
------------
Rilix Technology

Based on Ivan Baranov MaterialFavoriteButton

License
-------

```
Copyright 2019 Rilix Technology

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```